package sentiment_analysis;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author Dibay
 */
public class Sentiment_analysis {
    
    private static HashMap positive_bow = new HashMap(); 
    private static HashMap negative_bow = new HashMap(); 
    private static final String pos_training = "pos_training.csv";
    private static final String pos_testing = "pos_testing.csv";
    private static final String neg_training = "neg_training.csv";
    private static final String neg_testing = "neg_testing.csv";
    private static final String positive_dataset = "positive.csv";
    private static final String negative_dataset = "negative.csv";
//    public static void main(String[] args) throws IOException, InterruptedException {
//        
//        Dataset_builder("corpus.csv","dataset.csv",positive_dataset,negative_dataset);
//        /*Preprocessing class, for processing dataset, 
//        **before creating Bag of Words , such as erratic casing => change all words from CAPTAL to lower case
//        **Emotions => replace all emotions { SMILE => {>:] :-) :) ... Go to http://en.wikipedia.org/wiki/List_of_emoticons} 
//        **Punctuation => like finding "?????" or "!!!!!" 
//        **Stop_words => ignoring words such as "a","an","the",etc. go to :http://www.ranks.nl/resources/stopwords.html
//        **Removing pointers like "@" and hashtags in dataset => replace by "POINTER" keyword
//        **CHECKING for synonym words*/
//        
//        Preprocessing pre_proc =null;
//        pre_proc =   new Preprocessing(pos_training);
//        pre_proc.processing("Link");
//        pre_proc.processing("Pointer");
//        pre_proc.processing("erratic_casing");
//        pre_proc.processing("emotions");
//        pre_proc.processing("stop_words");
//        pre_proc.processing("special_char");
//        positive_bow = pre_proc.bow();
//        //preprocessing on neg_training
//        pre_proc = new Preprocessing(neg_training);
//        pre_proc.processing("Link");
//        pre_proc.processing("Pointer");
//        pre_proc.processing("erratic_casing");
//        pre_proc.processing("emotions");
//        pre_proc.processing("stop_words");
//        negative_bow = pre_proc.bow();
//        
//        //do the same preprocessing on testing set also to make it easier to print out occuracy
//        pre_proc = new Preprocessing(pos_testing);
//        pre_proc.processing("Link");
//        pre_proc.processing("Pointer");
//        pre_proc.processing("erratic_casing");
//        pre_proc.processing("emotions");
//        pre_proc.processing("stop_words");
//        pre_proc.processing("special_char");
//        pre_proc.Preprocessed_testingset(pos_testing);
//        pre_proc = new Preprocessing(neg_testing);
//        pre_proc.processing("Link");
//        pre_proc.processing("Pointer");
//        pre_proc.processing("erratic_casing");
//        pre_proc.processing("emotions");
//        pre_proc.processing("stop_words");
//        pre_proc.processing("special_char");
//        pre_proc.Preprocessed_testingset(neg_testing);
//        //End of preprocessing on testing set also, and having them in their specific files
//        
//        float accuracy =  new Core().freq_based(positive_bow, negative_bow, pos_testing, neg_testing);
//        System.out.println("Agent accuracy with: "+(accuracy*100)+"%\n");
//        System.out.println("Do you want to change the size of training set, and try again ?(1/0)\n");
//        Scanner scan = new Scanner(System.in); 
//        String answ= new Scanner(System.in).nextLine();
//        if(answ.equals("1"))
//        {
//            new Sentiment_analysis().main(null);
//        }
//        else if(answ.equals("0"))
//        {
//            
//            System.exit(1);
//        }
//        else
//        {
//            System.err.println("Command unrecognized\n");
//            System.exit(1);
//        }
//    }
    
    public static float returnvalue(int value,String method_name) throws IOException, InterruptedException {
        
        Dataset_builder("corpus.csv","dataset.csv",positive_dataset,negative_dataset,value);
        /*Preprocessing class, for processing dataset, 
        **before creating Bag of Words , such as erratic casing => change all words from CAPTAL to lower case
        **Emotions => replace all emotions { SMILE => {>:] :-) :) ... Go to http://en.wikipedia.org/wiki/List_of_emoticons} 
        **Punctuation => like finding "?????" or "!!!!!" 
        **Stop_words => ignoring words such as "a","an","the",etc. go to :http://www.ranks.nl/resources/stopwords.html
        **Removing pointers like "@" and hashtags in dataset => replace by "POINTER" keyword
        **CHECKING for synonym words*/
        
        Preprocessing pre_proc =null;
        pre_proc =   new Preprocessing(pos_training);
        pre_proc.processing("Link");
        pre_proc.processing("Pointer");
        pre_proc.processing("erratic_casing");
        pre_proc.processing("emotions");
        pre_proc.processing("stop_words");
        pre_proc.processing("special_char");
        positive_bow = pre_proc.bow();
        //preprocessing on neg_training
        pre_proc = new Preprocessing(neg_training);
        pre_proc.processing("Link");
        pre_proc.processing("Pointer");
        pre_proc.processing("erratic_casing");
        pre_proc.processing("emotions");
        pre_proc.processing("stop_words");
        negative_bow = pre_proc.bow();
        
        //do the same preprocessing on testing set also to make it easier to print out occuracy
        pre_proc = new Preprocessing(pos_testing);
        pre_proc.processing("Link");
        pre_proc.processing("Pointer");
        pre_proc.processing("erratic_casing");
        pre_proc.processing("emotions");
        pre_proc.processing("stop_words");
        pre_proc.processing("special_char");
        pre_proc.Preprocessed_testingset(pos_testing);
        pre_proc = new Preprocessing(neg_testing);
        pre_proc.processing("Link");
        pre_proc.processing("Pointer");
        pre_proc.processing("erratic_casing");
        pre_proc.processing("emotions");
        pre_proc.processing("stop_words");
        pre_proc.processing("special_char");
        pre_proc.Preprocessed_testingset(neg_testing);
        //End of preprocessing on testing set also, and having them in their specific files
        
       // float accuracy =  new Core().naive_bayes_classifier(positive_bow, negative_bow, pos_testing, neg_testing);
        float accuracy = 0;
        if(method_name.equals("Naive Bayes"))
        {
            accuracy =  new Core().naive_bayes_classifier(positive_bow, negative_bow, pos_testing, neg_testing);
        }
        else
        {
            accuracy =  new Core().freq_based(positive_bow, negative_bow, pos_testing, neg_testing);
        }
        //System.out.println("Agent accuracy with: "+(accuracy*100)+"%\n");
        return accuracy*100;
    }
    
    
    
   //Dataset builder is extracting all positive and negative tweets from our corpus 
    /**
     *
     * @author Dibay
     *///**************************************************************************************************************************Remove this int                                 
    private static void Dataset_builder(String List_file_path, String Data_file_path,String positive_dataset,String negative_dataset,int val) throws IOException, InterruptedException
    {
        Pattern pos_pattern = Pattern.compile("\"(apple|google|microsoft|twitter)\",\"positive\",\"[0-9]{18}\"");
        Pattern neg_pattern = Pattern.compile("\"(apple|google|microsoft|twitter)\",\"negative\",\"[0-9]{18}\"");
        Pattern ID_pattern = Pattern.compile("[0-9]{18}");
        Matcher matcher  =  null ;  
        Matcher matcher2  =  null ; 
        int positive_counter = 0 ;
        int negative_counter = 0 ;

        
        Hashtable data_table  =  new Hashtable();
        //input file reader 
        BufferedReader lf_reader = new BufferedReader(new FileReader(List_file_path)); 
        BufferedReader df_reader = new BufferedReader(new FileReader(Data_file_path));
        //output file writer
        BufferedWriter pos_write = new BufferedWriter(new FileWriter(positive_dataset));
        BufferedWriter neg_write = new BufferedWriter(new FileWriter(negative_dataset));
        String line = null;  
        //crating a hashtable of tweets with their ID , for checking if they are positive or negative
        // for creating a positive and negative csv files
        while((line= df_reader.readLine())!=null)
        {
            matcher = ID_pattern.matcher(line);           
            if(matcher.find())
            {
                String[] temp_data =  line.split("[0-9]{18},");
                data_table.put(matcher.group(0).toString(), temp_data[1].toString().toLowerCase());
            }
        }
        
        while((line = lf_reader.readLine())!=null)
        {
            matcher  =  pos_pattern.matcher(line);
            matcher2 = neg_pattern.matcher(line);
            if(matcher.find())
            {
                String temp_positive_line = matcher.group();
                matcher = ID_pattern.matcher(temp_positive_line);
                if(matcher.find())
                {
                    if(data_table.get(matcher.group(0))!=null)
                    {
                        pos_write.write("positive,"+data_table.get(matcher.group(0)).toString()+"\n");
                        positive_counter+=1;
                    }
                }
            }
            else if(matcher2.find())
            {
                String temp_negative_line = matcher2.group();
                matcher2 = ID_pattern.matcher(temp_negative_line);
                if((matcher2.find()))
                {
                    if(data_table.get(matcher2.group(0))!=null)
                    {
                        neg_write.write("negative,"+data_table.get(matcher2.group(0)).toString()+"\n");
                        negative_counter+=1;
                    }
                }
            }
            else
            {
                
            }
        }
        pos_write.close();
        neg_write.close();
        lf_reader.close();
        df_reader.close();
        training_test_set_builder(positive_dataset, negative_dataset,positive_counter,negative_counter,val);
        
//*********************UNCOMMENT THIS LINE        
        //System.out.println("Positive sentence counter: "+positive_counter+"\n"+"Negative sentence counter: "+negative_counter+"\n");
    }
    
    
    /**
     *
     * @author Dibay
     */
    //training set builder is creating two different file for training and testing set, for positive and negative
    
    //*********************************************************************************************************Remove this int
    private static void training_test_set_builder(String positive,String negative,int total_pos,int total_neg,int val) throws FileNotFoundException, IOException
    {
        
        BufferedReader total_pos_reader = new BufferedReader(new FileReader(positive));
        BufferedReader total_neg_reader = new BufferedReader(new FileReader(negative));
        BufferedWriter pos_training_write = new BufferedWriter(new FileWriter(pos_training));
        BufferedWriter pos_testing_write = new BufferedWriter(new FileWriter(pos_testing));
        BufferedWriter neg_training_write = new BufferedWriter(new FileWriter(neg_training));
        BufferedWriter neg_testing_write = new BufferedWriter(new FileWriter(neg_testing));
        //Uncomment this line
        //System.out.println("Enter your appropriate trainin set size, <3-97>:\n");
        //String training_set_persentage=  new Scanner(System.in).nextLine();
        float  a = total_pos/val;//Integer.parseInt(training_set_persentage);
        int pos_training_size=  (int)Math.abs(a);
        ArrayList<String> temp_tweet = new ArrayList<String>() ; 
        Random random = new Random();
        for (int i =0; i<total_pos; i++)
        {
            temp_tweet.add(total_pos_reader.readLine().toString());
        }
        int count=0;  
        while(count<pos_training_size)
        {
            int rand =  random.nextInt(temp_tweet.size());
            String temp=  temp_tweet.get(rand);
            if(temp!=null)
            {
                temp_tweet.set(rand, null);
                pos_training_write.write(temp+"\n");
                count++;
            }
        }
        for(int i=0; i< temp_tweet.size(); i++)
        {
            String temp=temp_tweet.get(i);
            if(temp!=null)
            {
                pos_testing_write.write(temp+"\n");
            }
        }
        temp_tweet.clear() ;
        //**************************UNCOMMENT This line, remove val
        a = total_neg/val;//Integer.parseInt(training_set_persentage);
        int neg_training_size=  (int)Math.abs(a);
        for (int i =0; i<total_neg; i++)
        {
            temp_tweet.add(total_neg_reader.readLine().toString());
        }
        count =0;
        while(count<neg_training_size)
        {
            int rand =  random.nextInt(temp_tweet.size());
            String temp=  temp_tweet.get(rand);
            if(temp!=null)
            {
                temp_tweet.set(rand, null);
                neg_training_write.write(temp+"\n");
                count++;
            }
        }
        for(int i=0; i< temp_tweet.size(); i++)
        {
            String temp=temp_tweet.get(i);
            if(temp!=null)
            {
                neg_testing_write.write(temp+"\n");
            }
        }
        temp_tweet =null;
        total_neg_reader.close();
        total_pos_reader.close();
        pos_testing_write.close();
        pos_training_write.close();
        neg_testing_write.close();
        neg_training_write.close();
    }
}
