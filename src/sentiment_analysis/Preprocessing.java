/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sentiment_analysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.crypto.dsig.spec.HMACParameterSpec;


/**
 *
 * @author Dibay
 */
public class Preprocessing {
    HashMap data_table = new HashMap();
    static HashMap stop_word_table = new HashMap();
    static HashMap positive_emotion_table = new HashMap();
    static HashMap negative_emotion_table = new HashMap();
    Matcher matcher = null ; 
    String line =null; 
    
    
    public static final String[] special_char=  new String[] {"\\","/","?","!","[","]",",","<",">","@","#","$","%","^","&","*",";","'","\""};
    public static final String[] positive_emotion= new String[] {">:]",":-)",":)",":o)",":]", ":3", ":c)" ,":>", "=]", "8)", "=)" ,":}", ":^)", ":?)",
    ">:D", ":-D", ":D", "8-D", "8D", "x-D", "xD", "X-D" ,"XD" ,"=-D" ,"=D", "=-3", "=3" ,"8-D", "B^D",":-))",":))",":*", ":^*" ,"( '}{' )"
     ,">;]", ";-)",";)", "*-)", "*)", ";-]", ";]", ";D", ";^)", ":-,"};
    public static final String[] negative_emotion = new String[] {">:[", ":-(", ":(",  ":-c", ":c", ":-<",  ":?C",
         ":<", ":-[", ":[", ":{", ">.>" ,"<.<", ">.<",":-||",":@",":'-(", ":'(", ":'-)", ":')","QQ",";(","(:",">:/",":-/",":-.", ":/", "=/",":S",">:\\", ":\\"};
    public static final String[] stop_words =  new String[] {"i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that", "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or","at", "by", "as","for", "with", "about","into", "through", "during","to", "from", "in", "out","then", "once", "here", "there", "when", "where", "why", "how", "all","so", "s",};
             
    /**
     *
     * @author Dibay
     */
    public Preprocessing(String dataset_file) throws FileNotFoundException, IOException
    {
        BufferedReader reader  =  new BufferedReader(new FileReader(dataset_file));
        
        
        //Filling stop_word hashtable
        for(int i = 0; i<stop_words.length ; i++)
        {
            stop_word_table.put(stop_words[i], i);
        }
        for(int i = 0; i<positive_emotion.length ; i++)
        {
            positive_emotion_table.put(positive_emotion[i], i);
        }
        for(int i = 0; i<negative_emotion.length ; i++)
        {
            negative_emotion_table.put(negative_emotion[i], i);
        }
        int count=0;
        while((line=reader.readLine())!=null)
        {
            data_table.put(count, line); 
            count+=1;  
        }
        reader.close();
    }
    
    //Check_non_word is for checking if we still have emotics in
    //tweets, we use this method to prevent extra loop, for checking 
    //emotics inside tweets
    /**
     *
     * @author Dibay
     */
    public void processing(String arg) 
    {
        if("Link".equals(arg))
        {
            Pattern link_pattern=  Pattern.compile("http://");
            String URL_pattern =  "((?:http|https)(?::\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s\"]*))";
            for(int i =0; i<data_table.size() ; i++)
            {
                line = data_table.get(i).toString();
                matcher= link_pattern.matcher(line);
                if(matcher.find())
                {  
                  line =line.replaceAll(URL_pattern," link ");
                  data_table.put(i, line);
                }
            }
        }
        else if("Pointer".equals(arg))
        {
            Pattern reference_pattern=  Pattern.compile("@");
            Pattern hash_pattern=  Pattern.compile("#");
            String Pointer_reference_pattern =  "@(\\w)+(\\.?)(?:[^\\s]*)";
            String Pointer_hash_pattern =  "#(\\w)+(\\.?)(?:[^\\s]*)";
          
            for(int i =0; i<data_table.size() ; i++)
            {
                line = data_table.get(i).toString();
                matcher= reference_pattern.matcher(line);
                if(matcher.find())
                {  
                  line =line.replaceAll(Pointer_reference_pattern," pointer ");
                
                }
                matcher= hash_pattern.matcher(line);
                if(matcher.find())
                {
                    line = line.replaceAll(Pointer_hash_pattern," pointer ");
                }
                data_table.put(i, line);
            }
        }
        else if("erratic_casing".equals(arg))
        {
            for(int i =0; i<data_table.size() ; i++)
            {
                line = data_table.get(i).toString();
                line =  line.toLowerCase() ; 
                data_table.put(i, line);
            }
        }
        else if("emotions".equals(arg))//for replacing emotions in tweets 
        {
            for(int i =0; i<data_table.size() ; i++)
            {
                line = data_table.get(i).toString();
                for(String str: line.split(" "))
                {
                    if(positive_emotion_table.get(str)!=null)
                    {
                        line= line.replace(str,"smily" );
                    }
                    else if(negative_emotion_table.get(str)!=null)
                    {
                        line = line.replace(str, "frown");
                    }
                }
                data_table.put(i, line);
            }
        }
        else if("stop_words".equals(arg))// for removing stop words like "a" , "the" etc. 
        {
            for(int i =0; i<data_table.size() ; i++)
            {
                line = data_table.get(i).toString();
                for(String str: line.split(" "))
                {
                    if(stop_word_table.get(str)!=null)
                    {
                       line= line.replace(str, "");
                    }
                }
            }
        }
              /********************************************************
                *****************CODE IS NOT EFFICIENT******************
                 * ************ CHANGE THE CODE HERE FOR ABBRIVATION*****
                 * *****************************************************/
        /*else if("special_char".equals(arg))
        {
            for(int i=0 ; i<data_table.size() ; i++)
            {
                line = data_table.get(i).toString();
                for(int j=0; j<special_char.length ; j++)
                {
                    if(line.contains(special_char[j]))
                    {
                       line= line.replaceAll(special_char[j]," ");
                    }
                }
          
                if(line.contains(" m"))
                {
                    line  = line.replace(" m","");
                }
                else if(line.contains(" re"))
                {
                    line = line.replace(" re", "");
                }
                else if(line.contains(" ve"))
                {
                    line = line.replace(" ve","");
                }
                else if(line.contains(" s"))
                {
                    line = line.replace(" s","");
                }
                else if(line.contains(" d"))
                {
                    line = line.replace(" d", "");
                }
                data_table.put(i, line);
            }
        }*/
        
    }
    /**
     *
     * @author Dibay
     */
    public HashMap bow()
    {
        HashMap bow =  new HashMap(); 
        for (int i=0;i<data_table.size(); i++)
        {
           String line =  data_table.get(i).toString();  
            if(line.contains("positive,"))
            {
                line  =  line.replace("positive,",""); 
            }
            else if (line.contains("negative,"))
            {
                line  =  line.replace("negative,",""); 
            }
            for (String str: line.split(" "))
            {
                if(bow.get(str)!=null)
                {
                    int value =  Integer.parseInt(bow.get(str).toString());
                    bow.put(str,value+1);
                }
                else
                {
                    bow.put(str, 1);
                }
            }
        }
        return bow;
    }
    public void Preprocessed_testingset(String filename) throws IOException
    {
        boolean delete_success=  (new File(filename).delete());
        if(!delete_success)
        {
            System.err.println("Couldn't remove file, IO ERROR\n");
        }
        BufferedWriter writer  =  new BufferedWriter(new FileWriter(filename));
        
        for(int i =0 ;  i<data_table.size(); i++)
        {
            String line = data_table.get(i).toString();
            if(line.contains("positive"))
            {
                line  =  line.replace("positive,",""); 
            }
            else if (line.contains("negative,"))
            {
                line  =  line.replace("negative,",""); 
            }
            writer.write(line+"\n");
        }
        writer.close();
        
    }
}
