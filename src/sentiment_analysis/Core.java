/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sentiment_analysis;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Template;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author Dibay
 */
public class Core {
    
   float[] temp_value;
   
   
/**
 *
 * @author Dibay
 */
    public float freq_based(HashMap positive, HashMap negative,String pos_testing, String neg_testing) throws FileNotFoundException, IOException
    {
        float total_pos_count=0;
        float total_neg_count=0;
        float pos_correct=0; 
        float neg_correct= 0;
        float pos_val = 0;
        float neg_val = 0;
        float accuracy =0 ;
        String line= null;
        //EW shouldn't use array here, becuse it's not fixed size, check each tweet limitation in 
        //term of Number of words
        
        
        BufferedReader reader =null;
        reader = new BufferedReader(new FileReader(pos_testing));
        while((line=reader.readLine())!=null)
        {
            total_pos_count+=1;
            int i=0;
            int max_size=0;
            for(String str: line.split(" "))
            {
                max_size+=1;
            }
            temp_value = new  float[max_size];
            for(String str: line.split(" "))
            {
                if(positive.get(str)!=null)
                {
                    pos_val = Integer.parseInt(positive.get(str).toString());
                }
                if(negative.get(str)!=null)
                {
                    neg_val = Integer.parseInt(negative.get(str).toString());
                }
                temp_value[i] = pos_val-neg_val;
                pos_val= neg_val=0;
                i+=1;
            }
            for (int j= 0;j<temp_value.length ; j++)
            {
                if((int) temp_value[j]>0)
                {
                    pos_val+=1;
                }
            }
            if(pos_val>=(temp_value.length/2))
            {
                pos_correct+=1;
            }
            pos_val= 0; 
            neg_val = 0 ; 
        }
        
        
        reader = new BufferedReader(new FileReader(neg_testing));
        while((line=reader.readLine())!=null)
        {
            total_neg_count+=1;
            int i=0;
            int max_size=0;
            for(String str: line.split(" "))
            {
                max_size+=1;
            }
            temp_value = new  float[max_size];
            for(String str: line.split(" "))
            {
                if(negative.get(str)!=null)
                {
                    neg_val = Integer.parseInt(negative.get(str).toString());
                }
                if(positive.get(str)!=null)
                {
                    pos_val = Integer.parseInt(positive.get(str).toString());
                }
                temp_value[i] = pos_val-neg_val;  
                pos_val= neg_val=0;
                i+=1;
            }
            for (int j= 0;j<temp_value.length ; j++)
            {
                 if((int)temp_value[j]<0)
                 {
                     neg_val+=1;
                 }
            }
            if(neg_val>=(temp_value.length/2))
            {
                neg_correct+=1;
            }
            pos_val= 0; 
            neg_val = 0 ; 
        }
        float pos_accuracy = pos_correct/total_pos_count;
        float neg_accuracy = neg_correct/total_neg_count;
        accuracy = (pos_accuracy+neg_accuracy)/2;
        return accuracy;
    }
 
    public float naive_bayes_classifier(HashMap positive,HashMap negative,String pos_training_address,String neg_training_address) throws FileNotFoundException,IOException
    { 
	float total_pos_count=0;
        float total_neg_count=0;
	float neg_prob=0;
	float pos_prob=0;
	float pos_correct=0; 
	float neg_correct= 0;
	float accuracy =0 ;
	String line= null;
	BufferedReader reader =null;
	reader = new BufferedReader(new FileReader(pos_training_address));
	while((line=reader.readLine())!=null)
	{
          total_pos_count+=1;
          HashMap<String,Integer> temp_hash =  new HashMap<String,Integer>() ;  
          for(String st:line.split(" ")) 
          {
            if(temp_hash.get(st)!=null)
            {
                temp_hash.put(st,(temp_hash.get(st)+1));
            }
            else
            {
                temp_hash.put(st,1); 
            }
          }
          neg_prob= naieve_bayes(positive,negative,temp_hash,line,"neg");
          pos_prob= naieve_bayes(positive,negative,temp_hash,line,"pos");
          int pos_p=  Math.abs((int)pos_prob);
          int neg_p= Math.abs((int)neg_prob);
          if(pos_p>neg_p)
          {
            pos_correct+=1;
          }
	}
	reader = new BufferedReader(new FileReader(neg_training_address));
	while((line=reader.readLine())!=null)
	{
            total_neg_count+=1;
            HashMap<String,Integer> temp_hash =  new HashMap<String,Integer>() ;  
            for(String st:line.split(" ")) 
            {
                    if(temp_hash.get(st)!=null)
                    {
                            temp_hash.put(st,(temp_hash.get(st)+1));
                    }
                    else
                    {
                            temp_hash.put(st,1); 
                    }
            }
            neg_prob= naieve_bayes(positive,negative,temp_hash,line,"neg");
            pos_prob= naieve_bayes(positive,negative,temp_hash,line,"pos");
            int pos_p=  Math.abs((int)pos_prob);
            int neg_p= Math.abs((int)neg_prob);
            if(neg_p>pos_p)
            {
                    neg_correct+=1;
            }
	}
	float pos_accuracy = pos_correct/total_pos_count;
	float neg_accuracy = neg_correct/total_neg_count;
	accuracy = (pos_accuracy+neg_accuracy)/2;
        return accuracy;
    }

    public float  naieve_bayes(HashMap positive,HashMap negative,HashMap temp,String line,String class_type)
    {
            float prob =0 ;  
            if(class_type.equals("neg"))
            {
                for (String st:line.split(" "))
                {
                        float fi= Float.parseFloat(temp.get(st).toString());
                        float neg_temp =0;
                        float pos_temp =0;
                        if(negative.get(st)!=null)
                        {
                            neg_temp =Float.parseFloat(negative.get(st).toString());
                        }
                        if(positive.get(st)!=null)
                        {
                            pos_temp = Float.parseFloat(positive.get(st).toString());
                        }
                        float ftotal= pos_temp+neg_temp;
                        float fc =0;
                        if(negative.get(st)!=null)
                        {
                            fc = Float.parseFloat(negative.get(st).toString());
                        }
                        float val = (fc+1)/(ftotal+2);
                        prob += fi * Math.log(val);// SUM fi * ln(fic+alpha/ftotal+(k*alpha)) => alpha is 1 here, and k is 2 , because we have 2 classes 
                }
            }
            else
            {
                for (String st:line.split(" "))
                {
                        float fi= Float.parseFloat(temp.get(st).toString());
                        float neg_temp =0;
                        float pos_temp =0;
                        if(negative.get(st)!=null)
                        {
                            neg_temp =Float.parseFloat(negative.get(st).toString());
                        }
                        if(positive.get(st)!=null)
                        {
                            pos_temp = Float.parseFloat(positive.get(st).toString());
                        }
                        float ftotal= pos_temp+neg_temp;
                        float fc =0;
                        if(positive.get(st)!=null)
                        {
                            fc= Float.parseFloat(positive.get(st).toString());
                        }
                        prob += fi * Math.log((fc+1)/(ftotal+2));// SUM fi * ln(fic+alpha/ftotal+(k*alpha)) => alpha is 1 here, and k is 2 , because we have 2 classes 
                }
            }
            return prob;
    }
}
